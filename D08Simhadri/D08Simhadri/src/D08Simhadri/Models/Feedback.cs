﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace D08Simhadri.Models
{
    public class Feedback
    {
        [ScaffoldColumn(false)]
        [Key]
        public int Feedback_ID { get; set; }

        [Display(Name = "Feedback Score")]
        public double feedbackscore { get; set; }

        public string CourseID { get; set; }
        public string Faculty_ID { get; set; }
        public string Student_ID { get; set; }

        [ScaffoldColumn(true)]
        public Int32? LocationID { get; set; }


        public virtual Location Location { get; set; }


        public List<Location> dealerLocation { get; set; }

        public static List<Feedback> ReadAllFromCSV(string filepath)
        {
            List<Feedback> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Feedback.OneFromCsv(v))
                                        .ToList();
            return lst;
        }


        public static Feedback OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Feedback item = new Feedback();

            int i = 0;
            item.feedbackscore = Convert.ToDouble(values[i++]);
            item.CourseID = Convert.ToString(values[i++]);
            item.Faculty_ID = Convert.ToString(values[i++]);
            item.Student_ID = Convert.ToString(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }


    }
}
