using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08Simhadri.Models;

namespace D08Simhadri.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08Simhadri.Models.Feedback", b =>
                {
                    b.Property<int>("Feedback_ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CourseID");

                    b.Property<string>("Faculty_ID");

                    b.Property<int?>("LocationID");

                    b.Property<string>("Student_ID");

                    b.Property<double>("feedbackscore");

                    b.HasKey("Feedback_ID");
                });

            modelBuilder.Entity("D08Simhadri.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<int?>("FeedbackFeedback_ID");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08Simhadri.Models.Feedback", b =>
                {
                    b.HasOne("D08Simhadri.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });

            modelBuilder.Entity("D08Simhadri.Models.Location", b =>
                {
                    b.HasOne("D08Simhadri.Models.Feedback")
                        .WithMany()
                        .HasForeignKey("FeedbackFeedback_ID");
                });
        }
    }
}
