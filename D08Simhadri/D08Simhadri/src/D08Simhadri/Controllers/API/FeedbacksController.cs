using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Simhadri.Models;

namespace D08Simhadri.Controllers
{
    [Produces("application/json")]
    [Route("api/Feedbacks")]
    public class FeedbacksController : Controller
    {
        private AppDbContext _context;

        public FeedbacksController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Feedbacks
        [HttpGet]
        public IEnumerable<Feedback> GetFeedbacks()
        {
            return _context.Feedbacks;
        }

        // GET: api/Feedbacks/5
        [HttpGet("{id}", Name = "GetFeedback")]
        public IActionResult GetFeedback([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Feedback feedback = _context.Feedbacks.Single(m => m.Feedback_ID == id);

            if (feedback == null)
            {
                return HttpNotFound();
            }

            return Ok(feedback);
        }

        // PUT: api/Feedbacks/5
        [HttpPut("{id}")]
        public IActionResult PutFeedback(int id, [FromBody] Feedback feedback)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != feedback.Feedback_ID)
            {
                return HttpBadRequest();
            }

            _context.Entry(feedback).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FeedbackExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Feedbacks
        [HttpPost]
        public IActionResult PostFeedback([FromBody] Feedback feedback)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Feedbacks.Add(feedback);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (FeedbackExists(feedback.Feedback_ID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetFeedback", new { id = feedback.Feedback_ID }, feedback);
        }

        // DELETE: api/Feedbacks/5
        [HttpDelete("{id}")]
        public IActionResult DeleteFeedback(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Feedback feedback = _context.Feedbacks.Single(m => m.Feedback_ID == id);
            if (feedback == null)
            {
                return HttpNotFound();
            }

            _context.Feedbacks.Remove(feedback);
            _context.SaveChanges();

            return Ok(feedback);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FeedbackExists(int id)
        {
            return _context.Feedbacks.Count(e => e.Feedback_ID == id) > 0;
        }
    }
}